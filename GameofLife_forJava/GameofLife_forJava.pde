import java.util.*;

Board board;
Cell current;

void setup()
{
  //fullScreen();
  size(450, 800);
  board = new Board(width, height);
}

void draw()
{
  background(0);
  board.calculateCells();
  board.draw();
  if (!board.checkMode()) {
    board.updateCells();
    delay(100);
  }
  fill(255);
  textSize(20);
  text("Days : "+board.getDays(), 20, height-20);
}



void mousePressed() {
  if (board.checkMode()) {
    current = board.getSelection();
    if (current!=null)
    {
      current.select();
      if (current.checkLive()) {
        current.makeDead();
        //println("Dead");
      }
      else {
        current.makeLive();
        //println("Alive");
      }
    }
  }
}

void mouseDragged() {
  if (board.checkMode()) {
    current = board.getSelection();
    if (current!=null)
    {
      current.select();
      if (current.checkLive()) {
        current.makeDead();
        //println("Dead");
      }
      else {
        current.makeLive();
        //println("Alive");
      }
    }
  }
}

void mouseReleased() {
  if (current!=null)
  {
    current.deselect();
  }
}

void keyPressed() {
  if (key == ' ') {
    board.clean();
    //println("Clean UP!");
  }
  else if (key == 'a') {
    board.changeMode();
    //println("Change Mode");
  }
  else if (key == 'w') {
    board.increaseScale(1);
    //println("Scale UP!");
  }
  else if (key == 's') {
    board.increaseScale(-1);
    //println("Scale DOWN!");
  }
  else if(key == 'q'){
    board.changeBackground();
  }
  
}

void keyReleased() {
  if (current!=null)
  {
    current.deselect();
  }
}