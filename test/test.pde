//import android.view.MotionEvent;
import ketai.ui.*;

KetaiGesture gesture;
Board board;
Cell cell;
ArrayList<Cell> cells;

void setup()
{ 
  size(300, 500); 
  //fullScreen();
  board = new Board();
  //cell = new Cell();
  //gesture = new KetaiGesture(this);
}

void draw()
{
  background(0);
  board.drawCells();
}
void keyPressed() {
  println("Tap");

  board.increaseScale(2);
  board.draw();
  board.drawCells();
}

void mousePressed() {
  println("mousePressed");
  for (Cell c : cells) {
    if (c.isOver(mouseX, mouseY)) {
      c.live = true;
      println(c.x, c.y);
      fill(0, 255, 0);
      rect(c.x, c.y, board.getCellsize(), board.getCellsize());
    };
  }
}


void onPinch(float x, float y, float d)
{ 
  println("pinch", x, y, d);
  //cell.increaseScale(d);
}


class Board {
  float defaultSize=10;
  float cellSize;
  float scaleFactor= 1;
  Board() {
    draw();
  }
  void draw() {
    cells = new ArrayList<Cell>();
    cellSize = defaultSize*scaleFactor;

    for (int i=0; i<100; i++) {
      for (int j=0; j<100; j++) {

        cells.add(new Cell(cellSize*i, cellSize*j, false));
      }
    }
  }
  void drawCells() {

    for (Cell c : cells) {
      c.drawcell(cellSize);
    }
  }
  //strokeWeight(2);
  //stroke(255);
  //fill(0);
  //rect(100, 100, 50*scaleFactor, 50*scaleFactor);


  float getCellsize() {
    return cellSize;
  }

  void increaseScale (float increment) {
    println(increment);
    if (increment>0) scaleFactor+= 0.05;
    else if (increment<0) scaleFactor-= 0.05;
  }
}



class Cell {
  //float defaultSize=10;
  //float scaleFactor= 1;
  float cellSize; 
  float x, y;
  boolean live;

  Cell(float x, float y, boolean live) {
    this.x=x;
    this.y=y;
    this.live = live;
  }

  boolean isOver(int mx, int my) {
    cellSize=board.getCellsize();

    if (mx<x) return false;
    if (mx>x+cellSize) return false;
    if (my<y) return false;
    if (my>y+cellSize) return false;
    return true;
  }
  //void increaseScale (float increment) {
  //  println(increment);
  //  if (increment>0) scaleFactor+= 0.05;
  //  else if (increment<0) scaleFactor-= 0.05;
  //}
  //float getCellsize() {
  //  cellSize = defaultSize*scaleFactor;
  //  return cellSize;
  //}

  void drawcell(float cellSize) {
    strokeWeight(1);
    stroke(255);
    if (live) fill(0, 255, 0);
    else fill(0);
    rect(x, y, cellSize, cellSize);
    //  }
    //}
  }
}