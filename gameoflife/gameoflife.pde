import android.view.MotionEvent;
import ketai.ui.*;
import ketai.sensors.*;
import java.util.Arrays;
//KetaiSensor sensor;
KetaiGesture gesture;

Board board;

//change between editing and other mode by pressing 'a'
//this will change to five finger on android. 

void setup() {
  fullScreen();
  board = new Board();
  gesture = new KetaiGesture(this);

  //sensor = new KetaiSensor(this);
  //sensor.start();
}

void draw() {
  background(0);
  board.drawCells();
  if (touches.length == 5) {
    board.changeEditMode();
  }

  if (board.isInEditMode == false) {
    board.updateCells();
    board.days++;
    delay(50);
  }
  fill(255);
  textSize(20);
  text("Days : "+board.getDays(), 20, height-20);
}

//x and y are center of pinch, d is distance apart
void onPinch(float x, float y, float d) {
  board.scaleOnPinch(d);
}
void onLongPress(float x, float y)
{
  board.changeEditMode();
}
void mousePressed() {

  if (board.isInEditMode == true) {
    board.paintOnTouch(mouseX, mouseY);
  }
}

void keyPressed() {
  if (key == 'a') {
    board.changeEditMode();
  }
}


//void onAccelerometerEvent(float x, float y, float z) {
//  //here we need to check for shaking so that we can call board.clear
//}

//Temporary printing for board.
void printMatrix(Cell[][] grid) {
  for (int r = 0; r < grid.length; r++) {
    for (int c = 0; c < grid[r].length; c++)
      System.out.print(grid[r][c] + " ");
    System.out.println();
  }
}
void mouseDragged() {
  if (board.isInEditMode == true) {
    board.paintOnTouch(mouseX, mouseY);
  }
}

/*
********Uncomment below function to have mouseX and mouseY = finger events on Android!*******
 */

public boolean surfaceTouchEvent(MotionEvent event) {

  //call to keep mouseX, mouseY, etc updated
  super.surfaceTouchEvent(event);

  //forward event to class for processing
  return gesture.surfaceTouchEvent(event);
}

class Board {
  private int boardWidth, boardHeight, days;
  private Cell [][] boardArray;
  private Cell [][] updateArray;
  int [] lastCellChanged;

  private boolean isInEditMode;

  Board() {
    isInEditMode = true;
    boardWidth = 10;
    boardHeight = height / (width / boardWidth);
    lastCellChanged = new int[2];

    boardArray = new Cell[boardHeight][boardWidth];
    updateArray = new Cell[boardHeight][boardWidth];

    populateCellArray(boardArray);
    populateCellArray(updateArray);
  }
  int getDays() {
    return days;
  }

  void populateCellArray(Cell[][] arr) {
    int rows = arr.length;
    int cols = arr[0].length;

    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
        int row = i;
        int col = j;

        arr[row][col] = new DeadCell(row, col, this);
      }
    }
  }

  void drawCells() {
    //println(this.boardArray.length);
    //println(this.boardArray[0].length);
    for (int i = 0; i < boardHeight; i++) {
      for (int j = 0; j < boardWidth; j++) {
        if (isLegalMove(i, j)) {
          Cell currCell = boardArray[i][j];
          if (currCell != null) {
            currCell.draw();
          }
        }
      }
    }
  }

  void paintOnTouch(float mx, float my) {

    //println(lastCellChanged);
    for (int i = 0; i < boardHeight; i++) {
      for (int j = 0; j < boardWidth; j++) {
        int row = i;
        int col = j;
        Cell currCell = boardArray[row][col];
        if (currCell != null) {
          if (currCell.isOver(mx, my)) {
            // println(row, col, lastCellChanged[0], lastCellChanged[1]);
            if (row == lastCellChanged[0] && col== lastCellChanged[1]) {
            } else {
              if (currCell.isAlive()) {
                boardArray[row][col] = new DeadCell(row, col, this);
                updateArray[row][col] = new DeadCell(row, col, this);
              } else {
                boardArray[row][col] = new LivingCell(row, col, this);
                updateArray[row][col] = new LivingCell(row, col, this);
              } 
              lastCellChanged[0] = i;
              lastCellChanged[1] = j;
            }
          }
        }

        //if (currCell.isOver(mx, my) && i != lastCellChanged[0] && j != lastCellChanged[1]) {
        //  if (currCell.isAlive()) {
        //    boardArray[row][col] = new DeadCell(row, col, this);
        //    updateArray[row][col] = new DeadCell(row, col, this);
        //  } else {
        //    boardArray[row][col] = new LivingCell(row, col, this);
        //    updateArray[row][col] = new LivingCell(row, col, this);
        //  }
        //}
      }
    }
  }


  void updateCells() {
    /*
    Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
     Any live cell with two or three live neighbours lives on to the next generation.
     Any live cell with more than three live neighbours dies, as if by overpopulation.
     Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
     */
    int[][] neighbourDirs = new int[][] {{-1, -1}, {-1, 0}, {-1, +1}, 
      { 0, -1}, { 0, +1}, 
      {+1, -1}, {+1, 0}, {+1, +1}
    };

    //loop through board array.
    for (int i = 0; i < boardHeight; i++) {
      for (int j = 0; j < boardWidth; j++) {
        int row = i;
        int col = j;
        int liveNeighborCount = 0;
        Cell currCell = boardArray[row][col];

        //if our cell is alive
        if (currCell.isAlive()) {

          //loop through all dirs
          for (int [] dir : neighbourDirs) {

            int newRow = row + dir[0];
            int newCol = col + dir[1];
            // if the dir is legal, lets check if its a lviing neighbor
            if (isLegalMove(newRow, newCol)) {
              if (boardArray[newRow][newCol].isAlive()) {
                liveNeighborCount++;
              }
            }
          }


          //adjust based on how many living neighbor cells we have
          if (liveNeighborCount < 2) {
            updateArray[row][col] = new DeadCell(row, col, this);
          } else if (liveNeighborCount > 3) {
            updateArray[row][col] = new DeadCell(row, col, this);
          }
        }

        //if its a dead cell
        else if (currCell.isAlive() == false) {
          //again count its neighbors
          for (int [] dir : neighbourDirs) {
            int newRow = row + dir[0];
            int newCol = col + dir[1];

            if (isLegalMove(newRow, newCol)) {
              if (boardArray[newRow][newCol].isAlive()) {
                liveNeighborCount++;
              }
            }
          }
          //3 neighbors, bring the cell back to life.
          if (liveNeighborCount == 3) {
            updateArray[row][col] = new LivingCell(row, col, this);
          }
        }
      }
    }
    //copy the changes we made to the update board to the main board. 
    transferUpdateToMain();
  }

  private void transferUpdateToMain() {
    for (int i = 0; i < boardHeight; i++) {
      for (int j = 0; j < boardWidth; j++) {
        int row = i;
        int col = j;

        if (boardArray[row][col].isAlive() != updateArray[row][col].isAlive()) {
          if (updateArray[row][col].isAlive()) {
            boardArray[row][col] = new LivingCell(row, col, this);
          } else {
            boardArray[row][col] = new DeadCell(row, col, this);
          }
        }
      }
    }
  }

  private boolean isLegalMove(int row, int col) {
    if (row >= 0 && row < boardHeight && col >= 0 && col < boardWidth) {
      return true;
    } 
    return false;
  }

  void scaleOnPinch(float pinchScalingFactor) {
    //print(this.boardWidth);
    //print(this.boardHeight);
    if ((pinchScalingFactor < 0) && (this.boardWidth > 9)) {
      this.boardWidth -= 1;
      boardHeight = height / (width / boardWidth);;
      boardArray = new Cell[boardHeight][boardWidth];
      updateArray = new Cell[boardHeight][boardWidth];

      populateCellArray(boardArray);
      populateCellArray(updateArray);
    } else if ((pinchScalingFactor > 0) && (this.boardWidth < 40)) {
      this.boardWidth += 1;
      boardHeight = height / (width / boardWidth);
      //this.boardHeight += 2;
      boardArray = new Cell[boardHeight][boardWidth];
      updateArray = new Cell[boardHeight][boardWidth];

      populateCellArray(boardArray);
      populateCellArray(updateArray);
    }
  }

  int getBoardWidth() {
    return boardWidth;
  }

  int getBoardHeight() {
    return boardHeight;
  }

  void boardClear() {
    populateCellArray(boardArray);
    populateCellArray(updateArray);
  }

  void changeEditMode() {
    this.isInEditMode = !isInEditMode;
    for (int i = 0; i < boardHeight; i++) {
      for (int j = 0; j < boardWidth; j++) {

        boardArray[i][j].changeEditMode();
        updateArray[i][j].changeEditMode();
      }
    }
  }
}

abstract class Cell {
  int x;
  int y;
  int cellWidth;
  int cellHeight;
  boolean isInEditMode;
  Cell(int row, int col, Board board) {
    cellWidth = width/board.getBoardWidth();
    cellHeight = cellWidth;
    
    //cellHeight = height / board.getBoardHeight();
    //cellWidth = cellHeight;
    
    //while (cellWidth * board.getBoardWidth() > width) {
    //  cellHeight -= 1;
    //  cellWidth = cellHeight;
    //}

    x = col;
    y = row;
    isInEditMode = board.isInEditMode;
  }
  boolean isOver(float mouseX, float mouseY) {
    if (cellWidth*x<mouseX && mouseX<cellWidth*(x+1)) {
      if (cellHeight*y<mouseY && mouseY<cellHeight*(y+1)) {
        return true;
      }
    }
    return false;
  }
  abstract boolean isAlive();
  void changeEditMode() {
    isInEditMode = !isInEditMode;
  }
  abstract void draw();
}

class LivingCell extends Cell {
  color livingCellColor;

  LivingCell(int row, int col, Board board) {
    super(row, col, board);
    livingCellColor = color(0, 255, 0);
  }

  void draw() {
    //println(isInEditMode);
    if (isInEditMode == true) {
      //println("stroked edit mode on");
      stroke(255, 255, 255);
      strokeWeight(2);
      fill(livingCellColor);
      rect(x*cellWidth, y*cellHeight, cellWidth, cellHeight);
      //Runmode
    } else {
      //println("nostroke edit mode off");
      noStroke();
      fill(livingCellColor);
      rect(x*cellWidth, y*cellHeight, cellWidth, cellHeight);
      //Editmode
    }
  }

  boolean isAlive() {
    return true;
  }
}

class DeadCell extends Cell {
  color deadCellColor;

  DeadCell(int row, int col, Board board) {
    super(row, col, board);
  }

  void draw() {
    //println(isInEditMode);
    if (isInEditMode == true) {
      //println("editing stroked");
      stroke(255, 255, 255);
      strokeWeight(2);
      fill(deadCellColor);

      rect(x*cellWidth, y*cellHeight, cellWidth, cellHeight);
    } else {
      //println("notEditingNoStroke");
      noStroke();
      fill(deadCellColor);
      rect(x*cellWidth, y*cellHeight, cellWidth, cellHeight);
    }
  }

  boolean isAlive() {
    return false;
  }
}