int defaultSize = 20;

class Board {
  private Cell originalCells[][];
  private Cell copyCells[][];
  int days;
  
  private boolean isInEditMode;
  private int boardWidth;
  private int boardHeight;
  private float scaleFactor;
  private float cellSize;
  public final float MIN_SCALE = 1;
  public int backgroundMode;
  
  private int row;
  private int col;
  
  Board(int boardWidth, int boardHeight) {
    days = 0;
    backgroundMode = 0;
    isInEditMode = false;
    scaleFactor = 1;
    cellSize = int(defaultSize * scaleFactor);
    this.boardWidth = boardWidth;
    this.boardHeight = boardHeight;
    row = int(boardWidth/cellSize);
    col = int(boardHeight/cellSize);
    originalCells = new Cell[row][col];
    copyCells = new Cell[row][col];
    for (int i=0; i<row; i++) {
      for (int j=0; j<col; j++) {
        originalCells[i][j] = new Cell(i*cellSize+cellSize/2, j*cellSize+cellSize/2, cellSize);
        copyCells[i][j] = new Cell(i*cellSize+cellSize/2, j*cellSize+cellSize/2, cellSize);
      }
    }
  }
  
 
  void draw() {
    image(backgroundMode(),0,0,width,height);
    println(backgroundMode);
    for (int i=0; i<row; i++) {
      for (int j=0; j<col; j++) {
        originalCells[i][j].draw();
      }
    }
  }
  
  void clean() {
    days = 0;
    for (int i=0; i<row; i++) {
      for (int j=0; j<col; j++) {
        originalCells[i][j].clean();
        copyCells[i][j].clean();
      }
    }
  }
  
  
  int countLiveNeighbors(Cell cells[][], int x, int y) {
    int counts = 0;
    int r = cells.length;
    int c = cells[0].length;
    for (int i=x-1; i<=x+1; i++) {
      for (int j=y-1; j<=y+1; j++) {
        if (!(i==x && j==y)) {
          if (i>=0 && i<r && j>=0 && j<c) {
            if (cells[i][j].checkLive())  counts += 1;
          }
        }
      }
    }
    return counts;
  }
  
  void calculateCells() {
    //arrayCopy(originalCells, copyCells);
    for (int i=0; i<row; i++) {
      for (int j=0; j<col; j++) {
        int numberOfLiveNeighbors = countLiveNeighbors(originalCells, i, j);
        if (originalCells[i][j].checkLive()) {  //a live cell
          if (numberOfLiveNeighbors < 2)  copyCells[i][j].makeDead();  // die caused by underpopulation
          else if (numberOfLiveNeighbors > 3)  copyCells[i][j].makeDead();  // die caused by oeverpopulation
          else  copyCells[i][j].makeLive();  //survive
        }
        else {  //a dead cell
          if (numberOfLiveNeighbors == 3)  copyCells[i][j].makeLive();  // revive by reproduction
          else  copyCells[i][j].makeDead();
        }
      }
    }
  }
  
  void updateCells() {
    for (int i=0; i<row; i++) {
      for (int j=0; j<col; j++) {
        if(copyCells[i][j].checkLive())  originalCells[i][j].makeLive();
        else  originalCells[i][j].makeDead();
      }
    }
    if (!isClean()) {
      days += 1;
    }
  }
  
  boolean isClean() {
    for (int i=0; i<row; i++) {
      for (int j=0; j<col; j++) {
        if(copyCells[i][j].checkLive())  return false;
      }
    }
    return true;
  }
  
  
  void changeMode() {
    if (isInEditMode)  isInEditMode = false;
    else  isInEditMode = true;
  }
  
  boolean checkMode() {
    return isInEditMode;
  }
  
    
  void changeBackground() {
    backgroundMode++;
    backgroundMode = backgroundMode%4;
  }
  
  PImage backgroundMode(){
    if(backgroundMode == 0) return loadImage("black.png");
    else if(backgroundMode == 1) return loadImage("Volcano.jpg");
    else if(backgroundMode == 2)return loadImage("sea.jpg");
    else return loadImage("Chris.png");
  }
  
  
  Cell getSelection() {
    for (int i=0; i<board.getRow(); i++) {
      for (int j=0; j<board.getCol(); j++) {
        if (originalCells[i][j].isOver(mouseX, mouseY)) {
          print("("+i+","+j+")"+" number of live neighbors is ");
          println(board.countLiveNeighbors(board.originalCells, i, j));
          return originalCells[i][j];
        }
      }
    }
    return null;
  }
  
  
  void increaseScale (float increment)
  {
    if (isInEditMode) {
      if (increment>0) scaleFactor+= 0.05;
      else if (increment<0) scaleFactor-= 0.05;
  
      if (scaleFactor<MIN_SCALE) scaleFactor= MIN_SCALE;
      cellSize = int(defaultSize * scaleFactor);
      row = int(boardWidth/cellSize);
      col = int(boardHeight/cellSize);
      cellSize = boardWidth/float(row);
      for (int i=0; i<row; i++) {
        for (int j=0; j<col; j++) {
          originalCells[i][j] = new Cell(i*cellSize+cellSize/2, j*cellSize+cellSize/2, cellSize);
          copyCells[i][j] = new Cell(i*cellSize+cellSize/2, j*cellSize+cellSize/2, cellSize);
        }
      }
    }
  }
  
  
  int getRow() {
    return row;
  }
  
  int getCol() {
    return col;
  }
  
  int getDays() {
    return days;
  }
    
}





class Cell {
  private float x, y;
  private float cellSize;
  private boolean live;
  
  private boolean selected;
  
  private float scaleFactor;
  
  Cell(float x, float y, float cellSize) {
    this.x = x;
    this.y = y;
    this.cellSize = cellSize;
    live = false;
    scaleFactor = 1;
    selected = false;
  }
  
  void draw() {
    pushMatrix();
    
    translate(x, y);
    //scale(scaleFactor);
    rectMode(CENTER);
    
    if (board.checkMode()) {
      stroke(255);  
      strokeWeight(1);
    }
    else {
      stroke(0);
      strokeWeight(0);
    }
    
    if (live){
      if(board.backgroundMode == 0) fill(255);
      else if(board.backgroundMode == 1) fill(255,0,0);
      else if(board.backgroundMode == 2) fill(0,255,255);
      else fill(150,255,0);
    }
    else  fill(0,0,0,50);
    rect(0, 0, cellSize, cellSize);
    
    popMatrix();
  }

  
  boolean isOver(int mx, int my)
  {
    // global to local coordinates
    PVector m= new PVector(mx, my);
    m.x-= x;
    m.y-= y;
    m.div(scaleFactor);

    // check
    if (m.x<-cellSize/2) return false; 
    if (m.x>cellSize/2) return false; 
    if (m.y<-cellSize/2) return false;
    if (m.y>cellSize/2) return false; 
    return true;
  }
  
  void clean() {
    live = false;
  }


  void select()
  {
    selected= true;
  }

  void deselect()
  {
    selected= false;
  }

  boolean isSelected()
  {
    return selected;
  }
  
  
  boolean checkLive() {
    return live;
  }
    
  void makeLive() {
    live = true;
  }
  
  void makeDead() {
    live = false;
  }
  
}