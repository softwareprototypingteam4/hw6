import java.util.*;
import android.view.MotionEvent;
import ketai.ui.*;
import ketai.sensors.*;

Board board;
Cell current;

KetaiSensor sensor;
KetaiGesture gesture;

void setup()
{
  fullScreen();
  //size(450, 800);
  board = new Board(width, height);
  
  gesture = new KetaiGesture(this);
  sensor = new KetaiSensor(this);
  sensor.start();
}

void draw()
{
  background(0);
  
  board.calculateCells();
  board.draw();
  
  if (!board.checkMode()) {
    board.updateCells();
    delay(100);
  }
  
  fill(255);
  textSize(30);
  text("Days : "+board.getDays(), 30, height-30);
}



//5 points multitouch
public boolean surfaceTouchEvent (MotionEvent event) 
{
  super.surfaceTouchEvent(event);

  int pointerIndex = event.getActionIndex();
  int pointerId = event.getPointerId(pointerIndex);
  int maskedAction = event.getActionMasked();

  switch (maskedAction) {

  case MotionEvent.ACTION_DOWN:
  case MotionEvent.ACTION_POINTER_DOWN: 
    {
      //println("DOWN "+pointerIndex);
      if (pointerIndex == 4) {
        board.changeMode();
        //println("Change Mode");
      }
      break;
    }
  case MotionEvent.ACTION_MOVE: 
    {
      //println("MOVE "+pointerIndex);
      break;
    }
  case MotionEvent.ACTION_UP:
  case MotionEvent.ACTION_POINTER_UP:
  case MotionEvent.ACTION_CANCEL: 
    {
      //println("UP "+pointerIndex);
      break;
    }
  }
  return gesture.surfaceTouchEvent(event);
}


void onPinch(float x, float y, float d) {
  if (touches.length == 2) {
    board.increaseScale(d);
    //println("pinch");
  }
}


void onDoubleTap(float x, float y) {
  board.changeBackground();
  //println("Change Background");
}



void onAccelerometerEvent(float x, float y, float z) {
  float accel = (pow(x,2) + pow(x,2) + pow(z,2))/ pow(9.8,2);
  if (accel > 10) {
    board.clean();
    //println("shakeDetected");
  }
  //println(accel);
}


void mousePressed() {
  if (board.checkMode()) {
    current = board.getSelection();
    if (current!=null)
    {
      current.select();
      if (current.checkLive()) {
        current.makeDead();
        //println("Dead");
      }
      else {
        current.makeLive();
        //println("Alive");
      }
    }
  }
}

void mouseDragged() {
  if (board.checkMode()) {
    current = board.getSelection();
    if (current!=null)
    {
      current.select();
      if (current.checkLive()) {
        current.makeDead();
        //println("Dead");
      }
      else {
        current.makeLive();
        //println("Alive");
      }
    }
  }
}


void mouseReleased() {
  if (current!=null)
  {
    current.deselect();
  }
}


void keyPressed() {
  if (key == ' ') {
    board.clean();
    //println("Clean UP!");
  }
}